def add_meanings(sentence, dico_searche):
	a = ""
	for e in sentence.split():
		if e in dico_searche:
			a += e +" "+ "("+dico_searche[e] +")" +" "
		else:
			a += e + " "
	return a


# 'she took the knife (pointy thing) to murder (unfriendly act) the red pepper (delicious condiment)'


print(add_meanings("she took the knife to murder the red pepper", {
            "murder": "unfriendly act",
            "pepper": "delicious condiment",
            "knife": "pointy thing",
            "SHE": "not in the sentence",
        }))
