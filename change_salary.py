def change_salary(salaries):
	for i in salaries.values():
		if not i["title"].startswith("C") or not i["title"].endswith("O"):
			i["salary"] = i["salary"] + 100
	return salaries

salaries = {
    "Elon Musk": { "title": "CEO", "salary": 5000 },
    "Alfred": { "title": "Secretary", "salary": 2000 },
    "Pascal": { "title": "Community manager", "salary": 1500 },
    "Picsou": { "title": "CFO", "salary": 3000 }
}


print(change_salary(salaries))