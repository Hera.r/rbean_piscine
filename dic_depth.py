def dic_depth(all_dict, count=0):
	if type(all_dict) != type(dict()):
		return count
	depths = []
	for k in all_dict:
		depths.append(dic_depth(all_dict[k], count + 1))
	return max(depths)


print(dic_depth({'one': 1, 'two': {'three': {'flat': 'entry'}}}))