def money_exchange(xchange_r):
	dico = {}
	for e in xchange_r:
		for k in xchange_r[e]:
			if k not in xchange_r:
				dico[k] = {e: round(1/xchange_r[e][k], 2)}
			else:
				dico[k][e] = round(1/xchange_r[e][k], 2)
	print(dico)

money_exchange({
    'usd': {
        'eur': 0.90,
        'rmb': 7.75,
        'vnd': 22000
    },
    'eur': {
        'yen': 125.00
    }
})


money_exchange({
    'usd': {
        'gbp': 1.46
    },
    'eur': {
        'gbp': 1.25
    }
})



# {'eur': {'usd': 1.11}, 'rmb': {'usd': 0.12}, 'vnd': {'usd': 0.0}, 'yen': {'eur': 0.0}}
# {'gbp': {'usd': 0.68, 'eur': 0.8}}
