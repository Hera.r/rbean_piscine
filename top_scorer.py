def top_scorer(data_match):
  dico = {}
  vide = []
  for k in data_match:
    for e in data_match[k]['goals']:
      if e not in dico:
        dico[e] = data_match[k]['goals'][e]
      else:
        dico[e] += data_match[k]['goals'][e]
  for k, v in dico.items():
    vide.append((k,v))
  vide.sort(key=lambda tup: tup[1], reverse=True)
  new_list = [i[0] for i in vide[0:3]]
  print(new_list)

data = {
    'argentina':
   	 {
   		 'duration': 98,
   		 'goals':{
   					     'paquito': 2,
                  'Couthino': 2
   			     }
   	   },
     'france':
   	 {
   		 'duration': 98,
   		 'goals':
   				  {
   					     'toto': 1,
                'Couthino': 2,
   					     'neymar': 1
   			     }
   	   }, 

     'angleterre':
   	 {
   		 'duration': 98,
   		 'goals':
   				  {
   					     'toto': 5,
                 'Couthino': 1,
   			     }
   	   }
} 

top_scorer(data)